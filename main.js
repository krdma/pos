// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')
const path = require('path')
const ThermalPrinter = require("node-thermal-printer").printer;
const PrinterTypes = require("node-thermal-printer").types;
const {PosPrinter} = require("electron-pos-printer");
const driver = require('@thiagoelg/node-printer');
const electron = typeof process !== 'undefined' && process.versions && !!process.versions.electron;

const options = {
   preview: false,               // Preview in window or print
   width: '170px',               //  width of content body
   margin: '0 0 0 0',            // margin of content body
   copies: 1,                    // Number of copies to print
   printerName: 'Raw-Queue',        // printerName: string, check it at webContent.getPrinters()
   timeOutPerLine: 400
}
 
const data = [
  {
      type: 'text',                                       // 'text' | 'barCode' | 'qrCode' | 'image'
      value: 'SAMPLE HEADING',
      style: `text-align:center;`,
      css: {"font-weight": "700", "font-size": "18px"}
   },{
      type: 'text',                       // 'text' | 'barCode' | 'qrCode' | 'image'
      value: 'Secondary text',
      style: `text-align:left;color: red;`,
      css: {"text-decoration": "underline", "font-size": "10px"}
   },{
      type: 'barCode',
      value: 'HB4587896',
      height: 12,                     // height of barcode, applicable only to bar and QR codes
      width: 1,                       // width of barcode, applicable only to bar and QR codes
      displayValue: true,             // Display value below barcode
      fontsize: 8,
   },{
     type: 'qrCode',
      value: 'https://github.com/Hubertformin/electron-pos-printer',
      height: 55,
      width: 55,
      style: 'margin: 10 20px 20 20px'
    }
]

function createWindow () {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
   mainWindow.webContents.openDevTools()
   let printer = new ThermalPrinter({
  type: PrinterTypes.Epson,    
  driver: require('@thiagoelg/node-printer'),
                                // Printer type: 'star' or 'epson'
 // interface: 'tcp://127.0.0.1',                       // Printer interface
  interface: 'printer:Epson-24-Pin',                       // Printer interface
  characterSet: 'PC437_USA',                                 // Printer character set - default: SLOVENIA
  removeSpecialCharacters: false,                           // Removes special characters - default: false
  lineCharacter: "=", 
                                        // Set character for lines - default: "-"
//  driver: require('printer'),
  options:{                                                 // Additional options
    timeout: 1000                                           // Connection timeout (ms) [applicable only for network printers] - default: 3000
  }
});
 
let isConnected =  printer.isPrinterConnected();       // Check if printer is connected, return bool of status
let raw =  printer.raw(Buffer.from("Hello world0"));    // Print instantly. Returns success or throws error
printer.raw(Buffer.from("Hello world22"));    // Print instantly. Returns success or throws error
printer.print("Hello World-1");                               // Append text
printer.println("Hello World-2");                             // Append text with new line
printer.println("Hello World1");                             // Append text with new line
printer.println("Hello World2");                             // Append text with new line
//printer.openCashDrawer();                                   // Kick the cash drawer
printer.print("Hello World");                               // Append text
printer.println("Hello World");                             // Append text with new line
                                 // Cuts the paper leaving a small bridge in middle (if printer supports multiple cut modes)
printer.beep();                                             // Sound internal beeper/buzzer (if available)
printer.upsideDown(true);                                   // Content is printed upside down (rotated 180 degrees)
printer.setCharacterSet("PC437_USA");                        // Set character set - default set on init
//printer.setPrinterDriver(Object)                            // Set printer drive - default set on init
 
printer.bold(true);                                         // Set text bold
printer.invert(true);                                       // Background/text color inversion
printer.underline(true);                                    // Underline text (1 dot thickness)
printer.underlineThick(true);                               // Underline text with thick line (2 dot thickness)
printer.drawLine();                                         // Draws a line
printer.newLine();                                          // Insers break line
 
printer.alignCenter();                                      // Align text to center
printer.println("Hello World2");                             // Append text with new line

printer.alignLeft();                                        // Align text to left
printer.println("Hello World2");                             // Append text with new line

printer.alignRight();                                       // Align text to right
printer.println("Hello World2");                             // Append text with new line
 
printer.setTypeFontA();                                     // Set font type to A (default)
printer.println("Hello World2");                             // Append text with new line
printer.setTypeFontB();                                     // Set font type to B
printer.println("Hello World2");                             // Append text with new line
 
printer.setTextNormal();                                    // Set text to normal
printer.println("Hello World2");                             // Append text with new line
printer.setTextDoubleHeight();                              // Set text to double height
printer.println("Hello World2");                             // Append text with new line
printer.setTextDoubleWidth();                               // Set text to double width
printer.println("Hello World2");                             // Append text with new line
printer.setTextQuadArea();                                  // Set text to quad area
printer.println("Hello World2");                             // Append text with new line
printer.setTextSize(7,7);                                   // Set text height (0-7) and width (0-7)
printer.println("Hello World2");                             // Append text with new line
 
printer.leftRight("Left", "Right");                         // Prints text left and right
printer.table(["One", "Two", "Three"]);                     // Prints table equaly
printer.tableCustom([                                       // Prints table with custom settings (text, align, width, cols, bold)
  { text:"Left", align:"LEFT", width:0.5 },
  { text:"Center", align:"CENTER", width:0.25, bold:true },
  { text:"Right", align:"RIGHT", cols:8 }
]);
 
//printer.code128("Code128");                                 // Print code128 bar code
printer.printQR("QR CODE");                                 // Print QR code
printer.printImage('./images/logo.png');  // Print PNG image
var data = "TEST"        // Barcode data (string or buffer)
var type = 7             // Barcode type (See Reference)
var settings = {         // Optional Settings
  characters: 1,         // Add characters (See Reference)
  mode: 3,               // Barcode mode (See Reference)
  height: 150,           // Barcode height (0≤ height ≤255)
}
 
printer.printBarcode(data, type, settings);
 /*
print.clear();                                              // Clears printText value
print.getText();                                            // Returns printer buffer string value
print.getBuffer();                                          // Returns printer buffer
print.setBuffer(newBuffer);                                 // Set the printer buffer to a copy of newBuffer
print.getWidth();      
//printer.cut();    
*/
let execute =  printer.execute();                      // Executes all the commands. Returns success or throws error
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()


  //  console.log('ass');
  /*
(async function () {
   // try {
let printer = new ThermalPrinter({
  type: PrinterTypes.Epson,                                  // Printer type: 'star' or 'epson'
  interface: 'tcp://127.0.0.1:9100',                       // Printer interface
  characterSet: 'SLOVENIA',                                 // Printer character set - default: SLOVENIA
  removeSpecialCharacters: false,                           // Removes special characters - default: false
  lineCharacter: "=",                                       // Set character for lines - default: "-"
  options:{                                                 // Additional options
    timeout: 5000                                           // Connection timeout (ms) [applicable only for network printers] - default: 3000
  }
});
 
let isConnected = await printer.isPrinterConnected();       // Check if printer is connected, return bool of status
let execute = await printer.execute();                      // Executes all the commands. Returns success or throws error
let raw = await printer.raw(Buffer.from("Hello world"));    // Print instantly. Returns success or throws error
printer.print("Hello World");                               // Append text
printer.println("Hello World");                             // Append text with new line
printer.openCashDrawer();                                   // Kick the cash drawer
printer.cut();                                              // Cuts the paper (if printer only supports one mode use this)
printer.partialCut();                                       // Cuts the paper leaving a small bridge in middle (if printer supports multiple cut modes)
printer.beep();                                             // Sound internal beeper/buzzer (if available)
printer.upsideDown(true);                                   // Content is printed upside down (rotated 180 degrees)
printer.setCharacterSet("SLOVENIA");                        // Set character set - default set on init
printer.setPrinterDriver(Object)                            // Set printer drive - default set on init
 
printer.bold(true);                                         // Set text bold
printer.invert(true);                                       // Background/text color inversion
printer.underline(true);                                    // Underline text (1 dot thickness)
printer.underlineThick(true);                               // Underline text with thick line (2 dot thickness)
printer.drawLine();                                         // Draws a line
printer.newLine();                                          // Insers break line
 
printer.alignCenter();                                      // Align text to center
printer.alignLeft();                                        // Align text to left
printer.alignRight();                                       // Align text to right
 
printer.setTypeFontA();                                     // Set font type to A (default)
printer.setTypeFontB();                                     // Set font type to B
 
printer.setTextNormal();                                    // Set text to normal
printer.setTextDoubleHeight();                              // Set text to double height
printer.setTextDoubleWidth();                               // Set text to double width
printer.setTextQuadArea();                                  // Set text to quad area
printer.setTextSize(7,7);                                   // Set text height (0-7) and width (0-7)
 
printer.leftRight("Left", "Right");                         // Prints text left and right
printer.table(["One", "Two", "Three"]);                     // Prints table equaly
printer.tableCustom([                                       // Prints table with custom settings (text, align, width, cols, bold)
  { text:"Left", align:"LEFT", width:0.5 },
  { text:"Center", align:"CENTER", width:0.25, bold:true },
  { text:"Right", align:"RIGHT", cols:8 }
]);
 
printer.code128("Code128");                                 // Print code128 bar code
printer.printQR("QR CODE");                                 // Print QR code
//await printer.printImage('./assets/olaii-logo-black.png');  // Print PNG image
 
print.clear();                                              // Clears printText value
print.getText();                                            // Returns printer buffer string value
print.getBuffer();                                          // Returns printer buffer
print.setBuffer(newBuffer);                                 // Set the printer buffer to a copy of newBuffer
print.getWidth();  
//} catch (e) {
//      console.log(e.stack);
 //   }
})()   
*/
})


// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
